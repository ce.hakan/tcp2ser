/*
 *
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

#include "io.h"

void main(void) {
    pid_t	childpid, pid;
    int 	status;
    char	pBuff[100];
    char  	*args[7] = {"", "", "", "", "", "", NULL};
    char	args0[10];
    char	args1[3];
    char	args2[3];
    char	args3[7];
    char	args4[3];
    char	args5[20];
    char	args6[11];
    FILE*       file;
    char	ttypar[50];
    int         mbaudrate;
    char	mparity;
    int         mport;
    char	mdatabits;
    char	mstopbit;
    char	baudrate[10];
    char	ports[10];
    char	parity[10];
    char	databits[10];
    char	stopbit[10];
    char 	x=0;


    file = fopen("/www/config/tcp2ser.txt", "r");
    char line[256];

    while (fgets(line, sizeof(line), file)) {
        if(x==0) sprintf(baudrate,"%s",line);
        if(x==1) sprintf(ports,"%s",line);
        if(x==2) sprintf(parity,"%s",line);
        if(x==3) sprintf(stopbit,"%s",line);
        if(x==4) sprintf(databits,"%s",line);

        //printf(line);
        x++;
    }

    fclose(file);

    mbaudrate = atoi(baudrate);
    mport     = atoi(ports);
    mparity   = parity[0];
    mdatabits = atoi(databits);
    mstopbit  = atoi(stopbit);

    if(mparity == 'N') {
        if(mdatabits == 8) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw cstopb\"", mbaudrate);
            }
        }
        if(mdatabits == 7) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw cs7\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw cs7 cstopb\"\0", mbaudrate);
            }
        }
    }

    if(mparity == 'E') {
        if(mdatabits == 8) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw parenb\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw parenb cstopb\"", mbaudrate);
            }
        }
        if(mdatabits == 7) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw cs7 parenb\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw cs7 parenb cstopb\"", mbaudrate);
            }
        }
    }

    if(mparity == 'O') {
        if(mdatabits == 8) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw parenb parodd\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw parenb parodd cstopb\"", mbaudrate);
            }
        }
        if(mdatabits == 7) {
            if(mstopbit == 1) {
                sprintf(ttypar,"\"%d raw cs7 parenb parodd\"", mbaudrate);
            }
            if(mstopbit == 2) {
                sprintf(ttypar,"\"%d raw cs7 parenb parodd\" cstopb", mbaudrate);
            }
        }
    }

    printf("Serial: %s %s %c %d %d\n", "/dev/ttyS1", ttypar, mparity, mdatabits, mstopbit);
    printf("TCP   : 0.0.0.0 %d\n", mport);

    strcpy((char*)args0, "remserial");
    sprintf(args1,"%s", "-d");
    sprintf(args2,"%s", "-p");
    sprintf(args3,"%d", mport);
    sprintf(args4,"%s", "-s");
    sprintf(args5,"%s", ttypar);
    sprintf(args6,"%s", "/dev/ttyS1");

    args[0] = &args0[0];
    args[1] = &args1[0];
    args[2] = &args2[0];
    args[3] = &args3[0];
    args[4] = &args4[0];
    args[5] = &args5[0];
    args[6] = &args6[0];

    sprintf(pBuff,"%s %s %s %s %s %s %s &\0", args0, args1, args2, args3, args4, ttypar, args6);
    printf("%s\r\n",pBuff);

    system(pBuff);

    write_OUTPUT('9','1');  // only for led blink
    usleep(500000);
    write_OUTPUT('9','0');  // only for led blink

    //while(1) sleep(10);

    /*
        for ( ; ; )
        {


                childpid = fork();

                if ( childpid == 0)	// child process
                {
                        system("remserial -d -p 5000 -s \"9600 raw\" /dev/ttyS1\0");
                        //system(pBuff);
                        //execvp( args[0], args);
                        exit(0);
                }
                if (childpid > 0)	// parent process
                {
                        if( (pid = wait(&status)) < 0){
                                printf("wait2");
                        }
                }
                else			// can not fork
                {
                        printf("fork failed");
                }

        }
        */

}
