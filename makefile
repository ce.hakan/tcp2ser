# the compiler: gcc for C program, define as g++ for C++
CC = mpisel-openwrt-linux-gcc

# compiler flags:
# -g adds debugging information to the executable file
# -Wall turns on mosti but not all, compiler warnings
CFLAGS = -g -Wall

# the build target executable:
TARGET = tcp2ser

OBJ = $(TARGET).o io.o

all: $(TARGET)

$(TARGET): $(OBJ)
        $(CC) $(CFLAGS) $(OBJ) -o $(TARGET)
        rm -f $(TARGET).o io.o

tcp2ser.o: tcp2ser.c io.h
        $(CC) $(CFLAGS) -c $(TARGET).c

io.o: io.c io.h
        $(CC) $(CFLAGS) -c io.c

clean:
        /bin/rm -f $(TARGET) $(TARGET).o io.o

install:
        scp tcp2ser root@192.168.100.1:/usr/bin/
